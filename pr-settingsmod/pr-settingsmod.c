/*
** pr-settingsmod.c -- modifies settings in /opt/PigRemote/etc/pigremote.settings
** (C) Nick Garner, Pignology, LLC, 2014
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libconfig.h>
#include <getopt.h>

int debug = 1;
static int verbose_flag;

//settings:
config_t config;
config_setting_t *root, *setting;

void showhelp()
{
  printf("\n>>>> settingsmod\n\n");
  printf("settingsmod is used by the webserver to modify settings from the webpage.\n");
  printf("You shouldn't need to use this.  Edit /opt/PigRemote/etc/pigremote.settings\n");
  printf("using nano or vi to change settings.\nLook at the annotated config for explanations.\n\n");

  printf("If you really want to use this, the command line options are:\n");
  printf("--audioroute <1|2> // 1=remote, 2=local\n");
  printf("--stunserver <string>\n");
  printf("--pigtailbaud <4800|9600|19200|38400>\n");
  printf("--pigtailstopbits <1|2>\n");
  printf("--srsscbaud <4800|9600|19200|38400>\n");
  printf("--pinginterval <#>\n");
  printf("--afraid_url <http string>\n");
  printf("\n");
  exit(0);
}


int main(int argc, char **argv)
{


  if (argc == 1)  showhelp();


    /* init libconfig */
    config_init(&config);
    if (! config_read_file(&config, "/opt/PigRemote/etc/pigremote.settings"))
    {
      fprintf(stderr, "%s:%d - %s\n", config_error_file(&config),
            config_error_line(&config), config_error_text(&config));
      config_destroy(&config);
      return(1);
    }

    /* get command line options */
    int c;
    while (1)
    {
/*
 Config Settings:
 audioroute - 1=remote, 2=local
 stunserver = string
 pigtailbaud - 4800, 9600, 19200, 38400
 pigtailstopbits - 1, 2
 srsscbaud - 4800, 9600, 19200, 38400
 pinginterval - int
*/
           static struct option long_options[] =
             {
               /* These options set a flag. */
               {"verbose", no_argument,       &verbose_flag, 1},
               /* These options don't set a flag.
                  We distinguish them by their indices. */
               {"audioroute",     required_argument,       0, 'a'},
               {"stunserver",  required_argument,       0, 'b'},
               {"pigtailbaud",  required_argument, 0, 'c'},
               {"pigtailstopbits",  required_argument, 0, 'f'},
               {"srsscbaud",  required_argument, 0, 'd'},
               {"pinginterval",    required_argument, 0, 'e'},
               {"afraid_url",    required_argument, 0, 'g'},
               {"help",    required_argument, 0, 'h'},
               {0, 0, 0, 0}
             };
           /* getopt_long stores the option index here. */
           int option_index = 0;
     
           c = getopt_long (argc, argv, "a:b:c:d:e:f:g:", long_options, &option_index);
     
           /* Detect the end of the options. */
           if (c == -1)
             break;
     
int i;
           switch (c)
             {
             case 0:
               /* If this option set a flag, do nothing else now. */
               if (long_options[option_index].flag != 0)
                 break;
               printf ("option %s", long_options[option_index].name);
               if (optarg)
                 printf (" with arg %s", optarg);
               printf ("\n");
               break;
     
             case 'a':
               //audioroute
               i = atoi(optarg);
               if (i < 1 || i > 2) { fprintf(stderr, "Audio route error, should be 1 or 2.\n"); exit(1); }
               printf ("Setting audio route to: %s\n", optarg);
               setting = config_lookup(&config, "pigremote.audio.route");
               config_setting_set_int(setting, atoi(optarg));
               break;
     
             case 'b':
               //stunserver
               printf ("Setting stunserver to: %s\n", optarg);
               setting = config_lookup(&config, "pigremote.audio.stunserver");
               config_setting_set_string(setting, optarg);
               break;
     
             case 'c':
               //pigtailbaud
               i = atoi(optarg);
               if (i != 4800 && i != 9600 && i != 19200 && i != 38400) { fprintf(stderr, "Pigtail baud error, should be 4800|9600|19200|38400.\n"); exit(1); }
               printf ("Setting pigtailbaud to: %s\n", optarg);
               setting = config_lookup(&config, "pigremote.serial.pigtailbaud");
               config_setting_set_int(setting, atoi(optarg));
               break;
     
             case 'f':
               //pigtailstopbits
               i = atoi(optarg);
               if (i != 1 && i != 2) { fprintf(stderr, "Pigtail stop bits error, should be 1|2.\n"); exit(1); }
               printf ("Setting pigtailstopbits to: %s\n", optarg);
               setting = config_lookup(&config, "pigremote.serial.pigtailstopbits");
               config_setting_set_int(setting, atoi(optarg));
               break;

             case 'd':
               //srsscbaud
               i = atoi(optarg);
	       if (i != 4800 && i != 9600 && i != 19200 && i != 38400) { fprintf(stderr, "SRS SC baud error, should be 4800|9600|19200|38400.\n"); exit(1); }
               printf ("Setting srsscbaud to: %s\n", optarg);
               setting = config_lookup(&config, "pigremote.serial.srsscbaud");
               config_setting_set_int(setting, atoi(optarg));
               break;
     
             case 'e':
               //pinginterval
               i = atoi(optarg);
               if (i < 1) { fprintf(stderr, "pinginteval error, should be greater than 0.\n"); exit(1); }
               printf ("Setting pinginterval to: %s\n", optarg);
               setting = config_lookup(&config, "pigremote.prannounce.pinginterval");
               config_setting_set_int(setting, atoi(optarg));
               break;

             case 'g':                                                                                                      
               //afraid_url
               printf ("Setting afraid_url to: %s\n", optarg);                                                              
               setting = config_lookup(&config, "pigremote.afraid_ddns.afraid_url");                                              
               config_setting_set_string(setting, optarg);                                                                  
               break;  
     
             case 'h':
               showhelp();
               break;

             case '?':
               /* getopt_long already printed an error message. */
               break;
     
             default:
               abort ();
             }
    }



    config_write_file(&config, "/opt/PigRemote/etc/pigremote.settings");
    
    return 0;
}
